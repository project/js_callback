##################
# IMPORTANT NOTE #
##################

Since Dec. 18th, 2012 we finally managed to re-merge with the "original" JS
callback handler module. We recommend replacing your current installation
with the ones can now download at http://drupal.org/project/js in order
to stay informed about the latest updates.

As clarified right from the beginning, there is no more need for this
module, hence it will no longer be maintained.

Thanks for your understanding!

##################

See major version branches for release packages.
